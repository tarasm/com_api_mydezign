<?php
/**
 */

defined('_JEXEC') or die;

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Api', JPATH_COMPONENT);
JLoader::register('ApiController', JPATH_COMPONENT . '/controller.php');

// Execute the task.
$controller = JControllerLegacy::getInstance('Api');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
