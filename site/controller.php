<?php
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controller');

/**
 * Class Phoca_interactivController
 *
 * @since  1.6
 */
class ApiController extends JControllerLegacy
{
	/** @var  JModelLegacy[] */
	public static $models;

	/**
	 * Method to display a view.
	 *
	 * @param   boolean $cachable  If true, the view output will be cached
	 * @param   mixed   $urlparams An array of safe url parameters and their variable types, for valid values see {@link JFilterInput::clean()}.
	 *
	 * @return  JController   This object to support chaining.
	 *
	 * @since    1.5
	 */
	public function display($cachable = false, $urlparams = false)
	{
		$view = JFactory::getApplication()->input->getCmd('view', '');
		JFactory::getApplication()->input->set('view', $view);

		if(method_exists($this, 'setupView' . ucfirst($view))){
			call_user_func_array(array($this, "setupView" . ucfirst($view)), array());
		}

		parent::display($cachable, $urlparams);

		return $this;
	}

	protected function setupViewSetimages(){
		$this->addModelPath($this->basePath . '/../com_phocagallery/models', 'PhocagalleryModel');

		$model = $this->getModel('Categories', 'PhocagalleryModel');
		$this->setModel2View($model);

		$this->phocaGaleryLoader();
	}

	protected function setupViewCategory(){
		$this->phocaGaleryLoader();

		$this->addModelPath($this->basePath . '/../com_phocagallery/models', 'PhocagalleryModel');
		$model = $this->getModel('Category', 'PhocagalleryModel');
		$this->setModel2View($model);

        $model->setId(JFactory::getApplication()->input->getInt('id',0));
	}

	public function getModel($name = '', $prefix = '', $config = array()) {
		$model = parent::getModel($name, $prefix, $config);
		if(!$model){
			return $model;
		}

		$class = get_class($model);
		$self_class = get_class($this);
		if(!empty($self_class::$models[$class])){
			return $self_class::$models[$class];
		}else{
			$self_class::$models[$class] = $model;
			return $model;
		}
	}

	protected function setModel2View($model){
		$document = JFactory::getDocument();
		$viewType = $document->getType();
		$viewName = $this->input->get('view', $this->default_view);
		$viewLayout = $this->input->get('layout', 'default', 'string');

		$view = $this->getView($viewName, $viewType, '', array('base_path' => $this->basePath, 'layout' => $viewLayout));

		$view->setModel($model, false);
	}

	protected function phocaGaleryLoader(){
		require_once(JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_phocagallery' . DS . 'libraries' . DS . 'loader.php');
		spl_autoload_register(array('loaderPhocaGaleryLibs', 'autoload'), true, true);
	}


}

class loaderPhocaGaleryLibs {
	public static function autoload($class) {
		if(!preg_match_all('/[A-Z]+[^A-Z]+/', $class, $matches, PREG_PATTERN_ORDER)){
			return;
		};

		$class = $matches[0];
		$prefix = array_shift($class) . array_shift($class);
		if ($prefix != 'PhocaGallery') {
			return;
		}

		$importLib = array(
			'phocagallery',
			strtolower($class[0]),
			strtolower(implode('', $class))
		);
		phocagalleryimport(implode('.', $importLib));

//		$libDir = strtolower($class[0]);
//		$file = strtolower(implode('', $class)) . '.php';
//
//		$testFile = JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_phocagallery' . DS . 'libraries'
//			. DS . 'phocagallery' . DS . $libDir . DS . $file;
//
//		if (JFile::exists($testFile)) {
//			include_once($testFile);
//		}
	}
}