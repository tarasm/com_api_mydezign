<?php
// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

if (!class_exists('JViewLegacy')){
    class JViewLegacy extends JView {

    }
}

class ApiViewCategory extends JViewLegacy
{
	function display($tpl = null)
	{
		$result = array();

		$tagId = JFactory::getApplication()->input->get( 'tagid', 0, 'int' );
		$model = $this->getModel('Category');
		$data = $model->getData(0, $tagId);
		$subCategoriesData = $model->getSubcategory();
		$category = $model->getCategory();

        $images = array();
        foreach($data as $imageData){
            $image = PhocaGalleryImageFront::displayCategoriesImageOrFolder($imageData->filename, 1, 1);

            $images[] = array(
                'id' => $imageData->id,
                'category_id' => $imageData->catid,
                'title' => $imageData->title,
                'src' => JUri::root(false) . $image->rel
            );
        }

        $subCategories = array();
        foreach($subCategoriesData as $subCategoryData){

        }

        $result = array(
            'id' => $category->id,
            'title' => $category->title,
            'description' => $category->description,
            'images' => $images,
            'subcategories' => $subCategories
        );

        print json_encode($result);
	}
}