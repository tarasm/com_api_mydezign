<?php
// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

if (!class_exists('JViewLegacy')){
    class JViewLegacy extends JView {

    }
}

class ApiViewSetimages extends JViewLegacy
{
	function display($tpl = null)
	{
		$result = array();

		$model = $this->getModel('Categories');
		$categories = $model->getData();

		phocagalleryimport('phocagallery.path.route');

        foreach($categories as $category){
            $image = PhocaGalleryImageFront::displayCategoriesImageOrFolder($category->filename, 1, 1);
            $images = array();
            if(file_exists(JPATH_ROOT . DS . $image->rel)){
                $images[] = array(
                    'src' => JUri::root(false) . $image->rel,
                    'title' => $category->title
                );
            }

            $link = JRoute::_( PhocaGalleryRoute::getCategoryRoute($category->id, $category->alias) );
            if(!preg_match('/^http/', $link)){
                $link = JUri::root(false) . $link;
                $link = preg_replace('/\/\/+/', '/', $link);
            }

            $result[] = array(
                    'id' => $category->id,
                    'title' => $category->title,
                    'parent_category_id' => $category->parent_id,
                    'link' => $link,
                    'images' => $images
            );

        }

        print json_encode($result);
	}
}